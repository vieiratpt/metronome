/*
 * Copyright (C) 2020 by João Pedro Vieira <vieiratpt@pm.me>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Metronome 1.0

ApplicationWindow {
    id: window
    title: "Metronome"
    visible: true
    height: 500
    width: 400

    property bool playing: false

    ColumnLayout {
        anchors.centerIn: parent

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: "Beats Per Minute"
        }
        RowLayout {
            id: beatsPerBarAttribution
            Layout.alignment: Qt.AlignHCenter
            RoundButton {
                text: "-"
                TapHandler {
                    onTapped: Metronome.setBeatsPerMinute(Metronome.beatsPerMinute-1)
                }
            }
            TextInput {
                Layout.alignment: Qt.AlignHCenter
                text: beatsPerMinuteSlider.value
                onEditingFinished: Metronome.setBeatsPerMinute(text)
            }
            RoundButton {
                text: "+"
                TapHandler {
                    onTapped: Metronome.setBeatsPerMinute(Metronome.beatsPerMinute+1)
                }
            }
        }
        Dial {
            id: beatsPerMinuteSlider
            Layout.alignment: Qt.AlignHCenter
            from: 20
            to: 250
            value: Metronome.beatsPerMinute
            onMoved: Metronome.setBeatsPerMinute(value)
            contentItem: Item {
                RoundButton {
                    id: tapButton
                    anchors.centerIn: parent
                    height: 90
                    width: 90
                    text: "TAP"
                    TapHandler {
                        onTapped: Metronome.tap()
                    }
                }
            }
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: "Beats Per Bar"
        }
        RowLayout {
            id: beatsPerBarSelection
            Layout.alignment: Qt.AlignHCenter
            RoundButton {
                text: "-"
                onClicked: {
                    Metronome.decrementBeatsPerBar()
                }
            }
            Repeater {
                model: Metronome.beatsPerBar
                delegate: CheckBox {
                    checked: {
                        if(Metronome.beatsSequencePosition(index) == 1) {
                            return Qt.Checked
                        }
                        else {
                            return Qt.Unchecked;
                        }
                    }
                    onCheckedChanged: {
                        if(checkState == Qt.Checked) {
                            Metronome.setBeatSequencePosition(index, 1)
                        }
                        else {
                            Metronome.setBeatSequencePosition(index, 0)
                        }
                    }
                }
            }
            RoundButton {
                text: "+"
                onClicked: Metronome.incrementBeatsPerBar()
            }
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: "Clicks Per Beat"
        }
        RowLayout {
            id: clicksPerBeatSelection
            Layout.alignment: Qt.AlignHCenter
            RadioButton {
                checked: true
                text: "1"
                onClicked: Metronome.setClicksPerBeat(1)
            }

            RadioButton {
                text: "2"
                onClicked: Metronome.setClicksPerBeat(2)
            }

            RadioButton {
                text: "3"
                onClicked: Metronome.setClicksPerBeat(3)
            }

            RadioButton {
                text: "4"
                onClicked: Metronome.setClicksPerBeat(4)
            }
        }

        RoundButton {
            id: playButton
            Layout.alignment: Qt.AlignHCenter
            text: playing ? "STOP" : "PLAY"
            onClicked: {
                if(playing) {
                    playing = false
                    Metronome.stop()
                }
                else {
                    playing = true
                    Metronome.play()
                }
            }
        }

        ProgressBar {
            Layout.alignment: Qt.AlignHCenter
            from: 0
            to: Metronome.beatsPerBar
            value: Metronome.beat
        }
    }
}
