/*
 * Copyright (C) 2020 by João Pedro Vieira <vieiratpt@pm.me>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "metronome.h"

Metronome::Metronome(QObject *parent) : QObject(parent) {
    setObjectName(QStringLiteral("Metronome"));
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &Metronome::beep);
    m_player = new QMediaPlayer(this);
    m_player->setVolume(50);
    m_beatsSequence.fill(1, 4);
}

Metronome::~Metronome() {
    deleteElapsedTimer();
    delete m_player;
}

quint8 Metronome::beatsPerBar() {
    return m_beatsSequence.length();
}

quint8 Metronome::clicksPerBeat() {
    return m_clicksPerBeat;
}

quint16 Metronome::beatsPerMinute() {
    return m_beatsPerMinute;
}

const int Metronome::beatsSequencePosition(int index) {
    return m_beatsSequence.at(index);
}

quint8 Metronome::beat() {
    return m_beat;
}

quint8 Metronome::click() {
    return m_click;
}

void Metronome::incrementBeatsPerBar() {
    m_beatsSequence.append(1);
    Q_EMIT beatsPerBarChanged();
}

void Metronome::decrementBeatsPerBar() {
    if(beatsPerBar() > 1) {
        m_beatsSequence.removeLast();
        Q_EMIT beatsPerBarChanged();
    }
}

void Metronome::setClicksPerBeat(quint8 value) {
    m_clicksPerBeat = value;
    updateInterval();
}

void Metronome::setBeatsPerMinute(quint16 value) {
    if(value >= MAX_BPM) {
        m_beatsPerMinute = MAX_BPM;
    }
    else if(value <= MIN_BPM) {
        m_beatsPerMinute = MIN_BPM;
    }
    else {
        m_beatsPerMinute = value;
    }
    updateInterval();
    Q_EMIT beatsPerMinuteChanged();
}

void Metronome::setBeatSequencePosition(int index, int value) {
    m_beatsSequence.replace(index, value);
}

void Metronome::setBeat(quint8 value) {
    m_beat = value;
    Q_EMIT beatChanged();
}

void Metronome::setClick(quint8 value) {
    m_click = value;
}

void Metronome::incrementBeat() {
    setBeat(beat() % beatsPerBar() + 1);
}

void Metronome::incrementClick() {
    setClick(click() % clicksPerBeat() + 1);
}

void Metronome::play() {
    deleteElapsedTimer();
    quint16 interval = 60000 / beatsPerMinute() / clicksPerBeat();
    setClick(clicksPerBeat());
    beep();
    m_timer->start(interval);
}

void Metronome::stop() {
    m_timer->stop();
    deleteElapsedTimer();
    setBeat(0);
}

void Metronome::tap() {
    if(!m_elapsedTimer) {
        m_elapsedTimer = new QElapsedTimer();
        m_elapsedTimer->start();
    }
    else {
        setBeatsPerMinute(60000 / m_elapsedTimer->elapsed());
        m_elapsedTimer->restart();
    }
}

void Metronome::beep() {
    if(click() == clicksPerBeat()) {
        incrementBeat();
    }
    incrementClick();

    if(beat() == 1 && click() == 1 && beatsSequencePosition(beat() - 1) == 1) {
        m_player->setMedia(QUrl(QStringLiteral("qrc:/beep_high.wav")));
        m_player->play();
    }
    else if(beatsSequencePosition(beat() - 1) == 1) {
        m_player->setMedia(QUrl(QStringLiteral("qrc:/beep_low.wav")));
        m_player->play();
    }
    // else: silence
}

void Metronome::updateInterval() {
    if(m_timer) {
        quint16 interval = 60000 / beatsPerMinute() / clicksPerBeat();
        m_timer->setInterval(interval);
    }
}

void Metronome::deleteElapsedTimer() {
    if(m_elapsedTimer) {
        delete m_elapsedTimer;
        m_elapsedTimer = NULL;
    }
}

QObject* Metronome::provider(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    Metronome *metronome = new Metronome();
    return metronome;
}
